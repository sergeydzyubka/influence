package sdzyubka.influence.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import sdzyubka.influence.core.service.Statistics;

import java.util.Map;

@Controller
@RequestMapping("/")
public class TwitterEndpoint {

    @Autowired
    private Statistics statistics;

    @RequestMapping(method = RequestMethod.POST)
    public String getInfluenceMap(@ModelAttribute(value = "username") String username, Model model) throws Exception {
        Map<String, Float> result = statistics.influence(username);
        model.addAttribute("map", result);
        model.addAttribute("username", username);
        model.addAttribute("count", result.size());
        model.addAttribute("total", result.entrySet().stream()
                .mapToDouble(entry -> entry.getValue()).sum());
        return "result";
    }

    @RequestMapping(method = RequestMethod.GET)
    public String get(Model model) {
        model.addAttribute("user", new User());
        return "index";
    }

    public static class User {
        private String username;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }
    }
}
