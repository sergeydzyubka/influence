package sdzyubka.influence.core.service;

import java.util.Map;
import java.util.Set;

/**
 * @author sdzyubka
 */
public interface SocialNetworkProvider {
    Map<String, Integer> repostCount(String username) throws Exception;

    Map<String, Integer> likeCount(String username) throws Exception;

    Map<String, Integer> mentionCount(String username) throws Exception;

    Set<String> follows(String username) throws Exception;

    Float averagePostActivity(String username);
}
