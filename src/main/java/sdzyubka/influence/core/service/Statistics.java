package sdzyubka.influence.core.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
public class Statistics {

    private static final Logger log = LogManager.getLogger(Statistics.class);

    private SocialNetworkProvider provider;

    @Value("${statistics.likeWeight}")
    private Integer likeWeight;

    @Value("${statistics.repostWeight}")
    private Integer repostWeight;

    @Value("${statistics.mentionWeight}")
    private Integer mentionWeight;

    @Value("${statistics.noFollowerDistance}")
    private Integer noFollowerDistance;

    @Value("${statistics.followerDistance}")
    private Integer followerDistance;

    @Autowired
    public void setProvider(SocialNetworkProvider provider) {
        this.provider = provider;
    }

    public Map<String, Float> influence(String influenced) throws Exception {
        Map<String, Float> influence = new HashMap<>();
        Set<String> follows = provider.follows(influenced);

        merge(likesPercentage(influenced), influence, likeWeight);
        merge(repostsPercentage(influenced), influence, repostWeight);
        merge(mentionsPercentage(influenced), influence, mentionWeight);

        influence.entrySet().stream().forEach(entry -> {
            try {
                entry.setValue(follows.contains(entry.getKey()) ?
                        entry.getValue() * provider.averagePostActivity(entry.getKey()) / followerDistance
                        : entry.getValue() * provider.averagePostActivity(entry.getKey()) / noFollowerDistance);
            } catch (Exception e) {
                log.error(e);
            }
        });

        return influence;
    }

    public Map<String, Float> likesPercentage(String username) throws Exception {
        Map<String, Integer> likesMap = provider.likeCount(username);
        Integer total = likesMap.entrySet().stream()
                .mapToInt(value -> value.getValue()).sum();

        Map<String, Float> resultMap = new HashMap<>();

        likesMap.entrySet().stream().forEach(entry -> resultMap.put(entry.getKey(),
                100 * Float.valueOf(entry.getValue()) / total));
        log.info("Likes percentage: " + resultMap);
        return resultMap;
    }

    public Map<String, Float> repostsPercentage(String username) throws Exception {
        Map<String, Integer> repostMap = provider.repostCount(username);
        Integer total = repostMap.entrySet().stream()
                .mapToInt(value -> value.getValue()).sum();

        Map<String, Float> resultMap = new HashMap<>();

        repostMap.entrySet().stream().forEach(entry -> resultMap.put(entry.getKey(),
                100 * Float.valueOf(entry.getValue()) / total));
        log.info("Reposts percentage: " + resultMap);
        return resultMap;
    }

    public Map<String, Float> mentionsPercentage(String username) throws Exception {
        Map<String, Integer> mentionsMap = provider.mentionCount(username);
        Integer total = mentionsMap.entrySet().stream()
                .mapToInt(value -> value.getValue()).sum();

        Map<String, Float> resultMap = new HashMap<>();

        mentionsMap.entrySet().stream().forEach(entry -> resultMap.put(entry.getKey(),
                100 * Float.valueOf(entry.getValue()) / total));
        log.info("Mentions percentage: " + resultMap);
        return resultMap;
    }

    private void merge(Map<String, Float> source, Map<String, Float> target, Integer criteriaWeight) {
        source.entrySet().stream().forEach(entry ->
                target.put(entry.getKey(), target.get(entry.getKey()) != null
                        ? target.get(entry.getKey()) + entry.getValue() * criteriaWeight
                        : entry.getValue() * criteriaWeight));
    }
}
