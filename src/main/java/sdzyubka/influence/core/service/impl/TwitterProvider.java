package sdzyubka.influence.core.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sdzyubka.influence.core.service.SocialNetworkProvider;
import twitter4j.Paging;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author sdzyubka
 */
@Service
public class TwitterProvider implements SocialNetworkProvider {

    private static final Logger log = LogManager.getLogger(TwitterProvider.class);

    private Twitter twitter;

    @Autowired
    public void setTwitter(Twitter twitter) {
        this.twitter = twitter;
    }

    @Override
    public Map<String, Integer> repostCount(String username) throws Exception {
        Map<String, Integer> retweetsMap = new HashMap<>();
        try {
            twitter.getUserTimeline(username, new Paging(1, 1000))
                    .stream()
                    .filter(status -> status.isRetweet())
                    .forEach(status ->
                            retweetsMap.put(status.getRetweetedStatus().getUser().getScreenName(),
                                    retweetsMap.get(status.getRetweetedStatus().getUser().getScreenName()) != null
                                            ? retweetsMap.get(status.getRetweetedStatus().getUser().getScreenName()) + 1
                                            : Integer.valueOf(1)));
        } catch (TwitterException e) {
            e.printStackTrace();
            throw new Exception(e);
        }
        return retweetsMap;
    }

    @Override
    public Map<String, Integer> likeCount(String username) throws Exception {
        Map<String, Integer> likesMap = new HashMap<>();
        try {
            twitter.getFavorites(username, new Paging(1, 1000))
                    .forEach(status ->
                            likesMap.put(status.getUser().getScreenName(), likesMap.get(status.getUser().getScreenName()) != null
                                    ? likesMap.get(status.getUser().getScreenName()) + 1 : Integer.valueOf(1)));
        } catch (TwitterException e) {
            e.printStackTrace();
            throw new Exception(e);
        }
        return likesMap;
    }

    @Override
    public Map<String, Integer> mentionCount(String username) throws Exception {
        Map<String, Integer> mentionsMap = new HashMap<>();
        try {
            twitter.getUserTimeline(username, new Paging(1, 1000)).stream()
                    .forEach(status ->
                            Stream.of(status.getUserMentionEntities())
                                    .forEach(mentionEntity -> {
                                        mentionsMap.put(mentionEntity.getScreenName(),
                                                mentionsMap.get(mentionEntity.getScreenName()) != null
                                                        ? mentionsMap.get(mentionEntity.getScreenName()) + 1
                                                        : Integer.valueOf(1));
                                    }));
        } catch (TwitterException e) {
            e.printStackTrace();
            throw new Exception(e);
        }
        return mentionsMap;
    }

    @Override
    public Set<String> follows(String username) throws Exception {
        return twitter.getFriendsList(username, -1, 1000)
                .stream().map(user -> user.getScreenName()).collect(Collectors.toSet());
    }

    @Override
    /**
     * @return average (posts/day)
     */
    public Float averagePostActivity(String username) {
        Float average = 0F;
        try {
            List<Status>statuses = twitter.getUserTimeline(username, new Paging(1, 50));
            Float interval = Float.valueOf((statuses.get(0).getCreatedAt().getTime() -
                    statuses.get(statuses.size() - 1).getCreatedAt().getTime()) / (24 * 60 * 60 * 1000));
            interval = interval == 0 ? 1 : interval;
            average = (Float.valueOf(statuses.size()) / interval);
            log.info(username+" "+average);
        }
        finally {
            return average;
        }
    }
}
