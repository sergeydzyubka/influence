package sdzyubka.influence.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 * @author sdzyubka
 */
@Configuration
public class AccessSocialNetworksConfig {

    @Value("${twitter.oauth.consumerKey}")
    private String twitterConsumerKey;

    @Value("${twitter.oauth.consumerSecret}")
    private String twitterConsumerSecret;

    @Value("${twitter.oauth.accessToken}")
    private String twitterAccessToken;

    @Value("${twitter.oauth.accessTokenSecret}")
    private String twitterAccessTokenSecret;

    @Bean
    public Twitter twitter(){
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey(twitterConsumerKey)
                .setOAuthConsumerSecret(twitterConsumerSecret)
                .setOAuthAccessToken(twitterAccessToken)
                .setOAuthAccessTokenSecret(twitterAccessTokenSecret);
        return new TwitterFactory(cb.build()).getInstance();
    }
}
